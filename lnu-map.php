<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              vk.com/lhospital
 * @since             1.0.0
 * @package           Lnu_Map
 *
 * @wordpress-plugin
 * Plugin Name:       LNU Map
 * Plugin URI:        https://wordpress.org/plugins/lnu-map/
 * Description:       LNU Maping Tool
 * Version:           1.0.0
 * Author:            bother
 * Author URI:        vk.com/lhospital
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lnu-map
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-lnu-map-activator.php
 */
function activate_lnu_map() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lnu-map-activator.php';
	Lnu_Map_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-lnu-map-deactivator.php
 */
function deactivate_lnu_map() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lnu-map-deactivator.php';
	Lnu_Map_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_lnu_map' );
register_deactivation_hook( __FILE__, 'deactivate_lnu_map' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-lnu-map.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_lnu_map() {

	$plugin = new Lnu_Map();
	$plugin->run();

}
run_lnu_map();
