<?php

/**
 * Fired during plugin deactivation
 *
 * @link       vk.com/lhospital
 * @since      1.0.0
 *
 * @package    Lnu_Map
 * @subpackage Lnu_Map/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Lnu_Map
 * @subpackage Lnu_Map/includes
 * @author     bother <harpandguitar@gmail.com>
 */
class Lnu_Map_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
