<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       vk.com/lhospital
 * @since      1.0.0
 *
 * @package    Lnu_Map
 * @subpackage Lnu_Map/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
    <form action="options.php" method="post">
    <div class="metabox-holder">
        <?php
            settings_fields( $this->plugin_name );
            do_settings_sections( $this->plugin_name );
            submit_button(__('Save all changes', $this->plugin_name), 'primary','submit', TRUE);
        ?>  
    </form>

			<div class="postbox">
				<h3><span><?php _e( 'Export Settings',$this->plugin_name ); ?></span></h3>
				<div class="inside">
					<p><?php _e( 'Export the plugin settings for this site as a .json file. This allows you to easily import the configuration into another site.',$this->plugin_name ); ?></p>
					<form method="post">
						<p><input type="hidden" name="pwsix_action" value="export_settings" /></p>
						<p>
							<?php wp_nonce_field( 'pwsix_export_nonce', 'pwsix_export_nonce' ); ?>
							<?php submit_button( __( 'Export',$this->plugin_name ), 'secondary', 'submit', false ); ?>
						</p>
					</form>
				</div><!-- .inside -->
			</div><!-- .postbox -->

			<div class="postbox">
				<h3><span><?php _e( 'Import Settings',$this->plugin_name ); ?></span></h3>
				<div class="inside">
					<p><?php _e( 'Import the plugin settings from a .json file. This file can be obtained by exporting the settings on another site using the form above.',$this->plugin_name ); ?></p>
					<form method="post" enctype="multipart/form-data">
						<p>
							<input type="file" name="import_file"/>
						</p>
						<p>
							<input type="hidden" name="pwsix_action" value="import_settings" />
							<?php wp_nonce_field( 'pwsix_import_nonce', 'pwsix_import_nonce' ); ?>
							<?php submit_button( __( 'Import',$this->plugin_name ), 'secondary', 'submit', false ); ?>
						</p>
					</form>
				</div><!-- .inside -->
			</div><!-- .postbox -->
		</div><!-- .metabox-holder -->
</div>