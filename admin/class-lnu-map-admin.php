<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       vk.com/lhospital
 * @since      1.0.0
 *
 * @package    Lnu_Map
 * @subpackage Lnu_Map/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Lnu_Map
 * @subpackage Lnu_Map/admin
 * @author     bother <harpandguitar@gmail.com>
 */
class Lnu_Map_Admin {

	/**
	 * The options name to be used in this plugin
	 *
	 * @since  	1.0.0
	 * @access 	private
	 * @var  	string 		$option_name 	Option name of this plugin
	 */
	private $option_name = 'lnu_map';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

        if (  get_current_screen()->id == 'settings_page_lnu-map' ) {
			wp_enqueue_style( 'chosen-css', plugin_dir_url( dirname(__FILE__) ) . 'includes/libs/chosen/chosen.min.css', array());	
			wp_enqueue_style( 'lnu-map-css', plugin_dir_url( dirname(__FILE__) ) . 'includes/css/lnu-map.min.css', array('chosen-css'), $this->version, 'all' );

			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/lnu-map-admin.css', array(), $this->version, 'all' );
		}
	}
	
	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

        if (  get_current_screen()->id == 'settings_page_lnu-map' ) {

			wp_enqueue_script( 'google-maps', 'http://maps.googleapis.com/maps/api/js?sensor=false', array(), false, true);
			wp_enqueue_script( 'chosen-js', plugin_dir_url( dirname(__FILE__) ) . 'includes/libs/chosen/chosen.jquery.min.js', array( 'jquery' ), false, true );
			wp_enqueue_script( 'gmaps-js', plugin_dir_url( dirname(__FILE__) ) . 'includes/libs/gmaps/gmaps.min.js', array( 'jquery', 'google-maps' ), false, true );		

			wp_enqueue_script( 'lnu-map-admin', plugin_dir_url( __FILE__ ) . 'js/lnu-map-admin.js', array( 'jquery', 'google-maps', 'gmaps-js', 'chosen-js' ), $this->version, true );
			
			// Localize the script with new data
			$data = array(
				'zoom' => get_option( 'lnu_map_zoom' ),
				'markers' => get_option( 'lnu_map_markers' ),
				'buildings' => get_option( 'lnu_map_buildings' ),
				'routes' => get_option( 'lnu_map_routes' ),
				'centerCoords' => get_option( 'lnu_map_center_coord' ),
				'centerText' => get_option( 'lnu_map_center_text' )
			);
			wp_localize_script( 'lnu-map-admin', 'phpData', $data );

		}
	}

	function process_settings_export() {
		if( empty( $_POST['pwsix_action'] ) || 'export_settings' != $_POST['pwsix_action'] )
			return;

		if( ! wp_verify_nonce( $_POST['pwsix_export_nonce'], 'pwsix_export_nonce' ) )
			return;

		if( ! current_user_can( 'manage_options' ) )
			return;

		$settings = array(
			'markers' => get_option( 'lnu_map_markers' ),
			'buildings' => get_option( 'lnu_map_buildings' ),
			'routes' => get_option( 'lnu_map_routes' )
		);

		ignore_user_abort( true );
		nocache_headers();

		header( 'Content-Type: application/json; charset=utf-8' );
		header( 'Content-Disposition: attachment; filename=lnu-map-data-export-' . date( 'm-d-Y' ) . '.json' );
		header( "Expires: 0" );
		echo json_encode($settings);
		exit;
	}

	function process_settings_import() {
		if( empty( $_POST['pwsix_action'] ) || 'import_settings' != $_POST['pwsix_action'] )
			return;

		if( ! wp_verify_nonce( $_POST['pwsix_import_nonce'], 'pwsix_import_nonce' ) )
			return;

		if( ! current_user_can( 'manage_options' ) )
			return;

		$tmp =  explode( '.', $_FILES['import_file']['name'] );
		$extension = end($tmp);
		if( $extension != 'json' ) {
			wp_die( __( 'Please upload a valid .json file' ) );
		}

		$import_file = $_FILES['import_file']['tmp_name'];
		if( empty( $import_file ) ) {
			wp_die( __( 'Please upload a file to import' ) );
		}

		// Retrieve the settings from the file and convert the json object to an array.
		$settings = json_decode( file_get_contents( $import_file ), true );
		extract($settings, EXTR_PREFIX_SAME, "wddx");

		update_option( 'lnu_map_markers', $markers );
		update_option( 'lnu_map_buildings', $buildings );
		update_option( 'lnu_map_routes', $routes );
	}

	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function add_options_page() {	
		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'LNU Map Settings', $this->plugin_name ),
			__( 'LNU Map', $this->plugin_name ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);	
	}

	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	public function display_options_page() {
		include_once 'partials/lnu-map-admin-display.php';
	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {
	
		$settings_link = array(
			'<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __('Settings', $this->plugin_name) . '</a>',
		);
		return array_merge(  $settings_link, $links );

	}

	/**
	 * Register plugin settings
	 *
	 * @since  1.0.0
	 */
	public function register_setting () {

		// Add general settings
		add_settings_section(
			$this->option_name . '_general',
			__( 'General', $this->plugin_name ),
			array( $this, $this->option_name . '_general_cb' ),
			$this->plugin_name
		);

		// add_settings_field(
		// 	$this->option_name . '_position',
		// 	__( 'Text position', 'lnu_map' ),
		// 	array( $this, $this->option_name . '_position_cb' ),
		// 	$this->plugin_name,
		// 	$this->option_name . '_general',
		// 	array( 'label_for' => $this->option_name . '_position' )
		// );

			add_settings_field(
				$this->option_name . '_zoom',
				__( 'Default map zoom', $this->plugin_name  ),
				array( $this, $this->option_name . '_zoom_cb' ),
				$this->plugin_name,
				$this->option_name . '_general',
				array( 'label_for' => $this->option_name . '_zoom' )
			);

			add_settings_field(
				$this->option_name . '_center_coord',
				__( 'Center map on', $this->plugin_name  ),
				array( $this, $this->option_name . '_center_coord_cb' ),
				$this->plugin_name,
				$this->option_name . '_general',
				array( 'label_for' => $this->option_name . '_center_coord' )
			);

			add_settings_field(
				$this->option_name . '_center_text',
				__( 'Map center label', $this->plugin_name  ),
				array( $this, $this->option_name . '_center_text_cb' ),
				$this->plugin_name,
				$this->option_name . '_general',
				array( 'label_for' => $this->option_name . '_center_text' )
			);



		// Add map settings
		add_settings_section(
			$this->option_name . '_map_data',
			__( 'Map data', $this->plugin_name ),
			array( $this, $this->option_name . '_map_data_cb' ),
			$this->plugin_name
		);

			add_settings_field(
				$this->option_name . '_markers',
				__( 'Markers', $this->plugin_name ),
				array( $this, $this->option_name . '_markers_cb' ),
				$this->plugin_name,
				$this->option_name . '_map_data',
				array( 'label_for' => $this->option_name . '_markers' )
			);

			add_settings_field(
				$this->option_name . '_buildings',
				__( 'Buildings', $this->plugin_name  ),
				array( $this, $this->option_name . '_buildings_cb' ),
				$this->plugin_name,
				$this->option_name . '_map_data',
				array( 'label_for' => $this->option_name . '_buildings' )
			);

			add_settings_field(
				$this->option_name . '_routes',
				__( 'Routes', $this->plugin_name  ),
				array( $this, $this->option_name . '_routes_cb' ),
				$this->plugin_name,
				$this->option_name . '_map_data',
				array( 'label_for' => $this->option_name . '_routes' )
			);

		register_setting( $this->plugin_name, $this->option_name . '_position');
		register_setting( $this->plugin_name, $this->option_name . '_zoom', 'intval' );
		register_setting( $this->plugin_name, $this->option_name . '_center_coord');
		register_setting( $this->plugin_name, $this->option_name . '_center_text');
		register_setting( $this->plugin_name, $this->option_name . '_markers' );
		register_setting( $this->plugin_name, $this->option_name . '_buildings' );
		register_setting( $this->plugin_name, $this->option_name . '_routes' );
	}

	/**
	 * Render the text for the general section
	 *
	 * @since  1.0.0
	 */
	public function lnu_map_general_cb() {
		echo '<p>' . __( 'Please change the settings accordingly.', $this->plugin_name  ) . '</p>';
	}

	/**
	 * Render the text for the general section
	 *
	 * @since  1.0.0
	 */
	public function lnu_map_map_data_cb() {
		echo '<p>' . __( 'Right click to add marker, double-click to remove', $this->plugin_name  ) . '</p>';
		?>

		<div id="lnu-map-editor"></div>

		<input type="hidden" id="marker-id">
		<input type="hidden" id="marker-group">

	
		<div class=" marker-settings">
			<div class="postbox">
				<h3><span><?php echo __( 'Marker Settings', $this->plugin_name  ) ?></span></h3>
			
				<div class="inside">
					<table >
						<tr valign="top">
							<th scope="row"><?php echo __( 'Name', $this->plugin_name  ) ?></th>
							<td><input style="width: 590px" id="marker-name" type="text"></td>
						</tr>

						<tr valign="top">
							<th scope="row"><?php echo __( 'lat', $this->plugin_name  ) ?>/<?php echo __( 'lng', $this->plugin_name  ) ?></th>
							<td>
								<input style="width: 290px" id="marker-lat" type="text">
								<input style="width: 290px" id="marker-lng" type="text">
							</td>
						</tr>

						<tr valign="top">
							<th scope="row"><?php echo __( 'Content', $this->plugin_name  ) ?></th>
							<td><input style="width: 590px" id="marker-content" type="text"></td>
						</tr>

						<tr valign="top">
							<th scope="row"><?php echo __( 'Link', $this->plugin_name  ) ?></th>
							<td><input style="width: 590px" id="marker-href" type="text"></td>
						</tr>


						<tr valign="top">
							<th scope="row"><?php echo __( 'Icon', $this->plugin_name  ) ?></th>
							<td>
							<select id="marker-icon-url">
								<option disabled> <?php echo __('Select icon' , $this->plugin_name ) ?></option>
								<option value="0"><?php echo __('Default' , $this->plugin_name ) ?></option>
								<option value="img/information.png">Information</option>
								<option value="img/tree.png">Tree</option>
								<option value="img/blank.png">blank</option>
								<option value="img/assemblyhall.png">assemblyhall</option>
								<option value="img/book.png">book</option>
								<option value="img/buffet.png">buffet</option>
								<option value="img/canteen.png">canteen</option>
								<option value="img/college.png">college</option>
								<option value="img/computers.png">computers</option>
								<option value="img/department.png">department</option>
								<option value="img/faculty.png">Faculty</option>
								<option value="img/flowers.png">flowers</option>
								<option value="img/freetime.png">freetime</option>
								<option value="img/gym.png">gym</option>
								<option value="img/hospital.png">hospital</option>
								<option value="img/information.png">information</option>
								<option value="img/location.png">location</option>
								<option value="img/mosquito.png">mosquito</option>
								<option value="img/museums.png">museums</option>
								<option value="img/obs.png">obs</option>
								<option value="img/parking.png">parking</option>
								<option value="img/parking-bicycle.png">parking-bicycle</option>
								<option value="img/postcollege.png">postcollege</option>
								<option value="img/residence.png">residence</option>
								<option value="img/routes.png">routes</option>
								<option value="img/running.png">running</option>
								<option value="img/swimming.png">swimming</option>
								<option value="img/telescope.png">telescope</option>
								<option value="img/tree.png">tree</option>
								<option value="img/virus.png">virus</option>
							</select>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row"><?php echo __( 'Category', $this->plugin_name  ) ?></th>
							<td>
								<!--<select id="markers-groups"></select>-->
														
								<input id="markers-groups" list="categories-list" name="markers-groups">
								<datalist id="categories-list">
		
								</datalist>
							</td>
						</tr>

						<tr valign="top">
							<td colspan="2" style="padding-top: 20px">
							
								<button id="marker-create" class="button button-secondary"> <?php echo __('Add new marker' , $this->plugin_name ) ?></button>
								<button id="marker-remove" class="button button-secondary"> <?php echo __('Remove selected marker' , $this->plugin_name ) ?></button>
							
							</td>
						</tr>

					</table>

				</div><!-- .inside -->

			</div><!-- .postbox -->
		</div>

		<?php
	}

	/**
	 * Render the radio input field for position option
	 *
	 * @since  1.0.0
	 */
	public function lnu_map_position_cb() {
		$position = get_option( $this->option_name . '_position' );
		?>
			<fieldset>
				<label>
					<input type="radio" name="<?php echo $this->option_name . '_position' ?>" id="<?php echo $this->option_name . '_position' ?>" value="before" <?php checked( $position, 'before' ); ?>>
					<?php _e( 'Before the content', $this->plugin_name ); ?>
				</label>
				<br>
				<label>
					<input type="radio" name="<?php echo $this->option_name . '_position' ?>" value="after" <?php checked( $position, 'after' ); ?>>
					<?php _e( 'After the content', $this->plugin_name ); ?>
				</label>
			</fieldset>
		<?php
	}

	/**
	 * Render the map zoom input for this plugin
	 *
	 * @since  1.0.0
	 */
	public function lnu_map_zoom_cb() {
		$zoom = get_option( $this->option_name . '_zoom' );
		echo '<input type="text" name="' . $this->option_name . '_zoom' . '" id="' . $this->option_name . '_zoom' . '" value="' . $zoom . '"> ';
	}

	public function lnu_map_center_coord_cb()
	{
		$center = get_option( $this->option_name . '_center_coord' );
		echo '<input type="text" style="width: 300px"  name="' . $this->option_name . '_center_coord' . '" id="' . $this->option_name . '_center_coord' . '" value="' . $center . '"> '
		. '<button class="button button-primary" id="center-use-marker">Use marker</button>';
	}

	public function lnu_map_center_text_cb()
	{
		$centerText = get_option( $this->option_name . '_center_text' );
		echo '<input type="text"  name="' . $this->option_name . '_center_text' . '" id="' . $this->option_name . '_center_text' . '" value="' . $centerText . '"> ';
	}

	/**
	 * Render the map markers textarea for this plugin
	 *
	 * @since  1.0.0
	 */
	public function lnu_map_markers_cb() {
	//	echo '<input type="hidden"  name="' . $this->option_name . '_markers' . '" id="' . $this->option_name . '_markers"/>';
		$markers = get_option( $this->option_name . '_markers' );
		

		echo '<textarea  cols="120" rows="5"  name="' . $this->option_name . '_markers' . '" id="' . $this->option_name . '_markers">' . $markers . '</textarea>' .
		'<br/><button class="button button-secondary" id="btn-update-markers">' . __( 'Update', $this->plugin_name ) . '</button>';
	}

	/**
	 * Render the map buildings textarea for this plugin
	 *
	 * @since  1.0.0
	 */
	public function lnu_map_buildings_cb() {
		$buildings = get_option( $this->option_name . '_buildings' );
		echo '<textarea cols="120" rows="5"  name="' . $this->option_name . '_buildings' . '" id="' . $this->option_name . '_buildings">' . $buildings . '</textarea>' .
		'<br/><button class="button button-secondary" id="btn-update-buildings">' . __( 'Update', $this->plugin_name ) . '</button>';
	}

	/**
	 * Render the map routes textarea for this plugin
	 *
	 * @since  1.0.0
	 */
	public function lnu_map_routes_cb() {
		$routes = get_option( $this->option_name . '_routes' );
		echo '<textarea cols="120" rows="5"  name="' . $this->option_name . '_routes' . '" id="' . $this->option_name . '_routes">' . $routes . '</textarea>' .
		'<br/><button class="button button-secondary" id="btn-update-routes">' . __( 'Update', $this->plugin_name ) . '</button>';
	}
}

?>