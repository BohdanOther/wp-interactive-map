��    $      <  5   \      0     1  	   @     J     S     a     i     q     �     �     �          !     &     -  �   =     �     �     �     �     �     �            '        B      a     �  1   �     �     �     �     �     �     �       �    $   �     �     	  $    	  
   E	     P	  7   e	     �	  !   �	  �   �	     �
     �
     �
  *   �
  �      #   �  >   �     �  "   	     ,  '   @     h  
   w  Q   �  V   �  J   +  ,   v  �   �  
   ]     h     �     �     �     �     �                                                                          
         	   "                                                !   #         $                                  Add new marker Buildings Category Center map on Content Default Default map zoom Export Export Settings Export the plugin settings for this site as a .json file. This allows you to easily import the configuration into another site. General Icon Import Import Settings Import the plugin settings from a .json file. This file can be obtained by exporting the settings on another site using the form above. LNU Map LNU Map Settings Link Map center label Map data Marker Settings Markers Name Please change the settings accordingly. Please upload a file to import Please upload a valid .json file Remove selected marker Right click to add marker, double-click to remove Routes Save all changes Select icon Settings Update lat lng Project-Id-Version: LNU Map
POT-Creation-Date: 2017-05-31 04:13+0300
PO-Revision-Date: 2017-05-31 04:22+0300
Last-Translator: 
Language-Team: 
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
X-Poedit-WPHeader: lnu-map.php
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Додати новий маркер Будівлі Категорія Центрувати карту на Вміст Стандартна Масштаб карти по замовчуванню Експортувати Експортувати дані Експортувати дані карти як файл формату json. Це дозволить просто перенести дані карти з одного сайту на інший. Загальні Іконка Імпортувати Імпортувати дані карти Імпортувати дані карти з json файлу. Файл можна отримати виконавши експортування. Інтерактивна карта Налаштування інтерактивної карти Посилання Текст центру карти Дані карти Налаштування маркера Маркери Назва Будь ласка, зробіть відповідні налаштування Спершу завантажте файл для імпортування даних. Будь ласка,  завантажте валідний json файл. Видалити обраний маркер Натисніть праву кнопку миші, щоб додати маркер; натисніть двічі ліву кнопку миші, щоб видалити маркер Шляхи Зберегти зміни Обрати іконку Налаштування Оновити Широта Довгота 