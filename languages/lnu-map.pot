#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: LNU Map\n"
"POT-Creation-Date: 2017-05-31 04:13+0300\n"
"PO-Revision-Date: 2017-05-31 04:13+0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: lnu-map.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: admin/class-lnu-map-admin.php:149
msgid "Please upload a valid .json file"
msgstr ""

#: admin/class-lnu-map-admin.php:154
msgid "Please upload a file to import"
msgstr ""

#: admin/class-lnu-map-admin.php:173
msgid "LNU Map Settings"
msgstr ""

#. Plugin Name of the plugin/theme
#: admin/class-lnu-map-admin.php:174
msgid "LNU Map"
msgstr ""

#: admin/class-lnu-map-admin.php:198
msgid "Settings"
msgstr ""

#: admin/class-lnu-map-admin.php:214
msgid "General"
msgstr ""

#: admin/class-lnu-map-admin.php:230
msgid "Default map zoom"
msgstr ""

#: admin/class-lnu-map-admin.php:239
msgid "Center map on"
msgstr ""

#: admin/class-lnu-map-admin.php:248
msgid "Map center label"
msgstr ""

#: admin/class-lnu-map-admin.php:260
msgid "Map data"
msgstr ""

#: admin/class-lnu-map-admin.php:267
msgid "Markers"
msgstr ""

#: admin/class-lnu-map-admin.php:276
msgid "Buildings"
msgstr ""

#: admin/class-lnu-map-admin.php:285
msgid "Routes"
msgstr ""

#: admin/class-lnu-map-admin.php:307
msgid "Please change the settings accordingly."
msgstr ""

#: admin/class-lnu-map-admin.php:316
msgid "Right click to add marker, double-click to remove"
msgstr ""

#: admin/class-lnu-map-admin.php:327
msgid "Marker Settings"
msgstr ""

#: admin/class-lnu-map-admin.php:332
msgid "Name"
msgstr ""

#: admin/class-lnu-map-admin.php:337
msgid "lat"
msgstr ""

#: admin/class-lnu-map-admin.php:337
msgid "lng"
msgstr ""

#: admin/class-lnu-map-admin.php:345
msgid "Content"
msgstr ""

#: admin/class-lnu-map-admin.php:350
msgid "Link"
msgstr ""

#: admin/class-lnu-map-admin.php:356
msgid "Icon"
msgstr ""

#: admin/class-lnu-map-admin.php:359
msgid "Select icon"
msgstr ""

#: admin/class-lnu-map-admin.php:360
msgid "Default"
msgstr ""

#: admin/class-lnu-map-admin.php:396
msgid "Category"
msgstr ""

#: admin/class-lnu-map-admin.php:410
msgid "Add new marker"
msgstr ""

#: admin/class-lnu-map-admin.php:411
msgid "Remove selected marker"
msgstr ""

#: admin/class-lnu-map-admin.php:437
msgid "Before the content"
msgstr ""

#: admin/class-lnu-map-admin.php:442
msgid "After the content"
msgstr ""

#: admin/class-lnu-map-admin.php:482 admin/class-lnu-map-admin.php:493
#: admin/class-lnu-map-admin.php:504
msgid "Update"
msgstr ""

#: admin/partials/lnu-map-admin-display.php:24
msgid "Save all changes"
msgstr ""

#: admin/partials/lnu-map-admin-display.php:29
msgid "Export Settings"
msgstr ""

#: admin/partials/lnu-map-admin-display.php:31
msgid ""
"Export the plugin settings for this site as a .json file. This allows you to "
"easily import the configuration into another site."
msgstr ""

#: admin/partials/lnu-map-admin-display.php:36
msgid "Export"
msgstr ""

#: admin/partials/lnu-map-admin-display.php:43
msgid "Import Settings"
msgstr ""

#: admin/partials/lnu-map-admin-display.php:45
msgid ""
"Import the plugin settings from a .json file. This file can be obtained by "
"exporting the settings on another site using the form above."
msgstr ""

#: admin/partials/lnu-map-admin-display.php:53
msgid "Import"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://wordpress.org/plugins/lnu-map/"
msgstr ""

#. Description of the plugin/theme
msgid "LNU Maping Tool"
msgstr ""

#. Author of the plugin/theme
msgid "bother"
msgstr ""

#. Author URI of the plugin/theme
msgid "vk.com/lhospital"
msgstr ""
