(function($) {
    'use strict';

    // is used when building path to media files;
    // e.g. markerIconUrl = absoluteImagesPath + 'lnuBuilding.png'
    var absoluteImagesPath = 'http://www.lnu.edu.ua/wp-content/themes/lnu-main/lib/map/'; // script_vars.images_folder;

    var markers = JSON.parse(phpData.markers);
    var routes = JSON.parse(phpData.routes);
    var buildings = JSON.parse(phpData.buildings);
    var centerCoords = (phpData.centerCoords || '49.840259, 24.022331').split(',');
    var centerText = phpData.centerText || 'Головний корпус';

    // main GMaps global object
    // to get actual google.maps.Map use map.map
    var map;

    // map center
    var lnu = new google.maps.LatLng(Number(centerCoords[0]), Number(centerCoords[1]));

    /**
     * @param {String} div - Div id where map should be placed.
     * @param {google.maps.LatLng} center - Map center.
     * 
     * All google.maps.Map options and events are supported.
     */
    var mapOptions = {
        div: '#lnu-map',
        center: lnu,
        zoom: parseInt(phpData.zoom) || 16,
        scaleControl: false,
        mapTypeControl: false,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        }
    };

    var chosenAutocompleteOptions = {
        no_results_text: "Нічого не знайдено!",
        width: "100%"
    };

    // markers arrays
    var markersBag = [];

    // polylines array
    var routesBag = [];

    // global infoWindow ensures one is open at time
    var infoWindow = new google.maps.InfoWindow();

    // global marker object ensures one marker is open at time
    // when building polygon is clicked
    var selectedBuldingMarkers = [];

    // default marker size
    var markerSize = new google.maps.Size(32, 37);

    // cached DOM.
    // initialized after custom controls is ready
    var $routesControl = {};
    var $markersSelect = {};
    var $routesToggle = {};

    var routesSchemesOverlaysCount = 0;
    // adjacent colors assumed to differ from each other a lot
    var routesSchemesColors = [
        "0000FF", "FF0000", "FF00FF", "AAAA00", "800000", "008000", "CCCC00", "66FF00", "CC3300", "9B6800",
        "000080", "008080", "800080", "808000", "000000", "8B4726", "6959CD", "473425", "FF7F24", "0099FF",
        "EE6363", "FF8247", "FFA500", "EE6AA7", "FF0033", "00CC99", "999966", "333333"
    ];
    /**
     * Returnt color by color from routesSchemesColors;
     * when array end reached loops from beginning.
     */
    function getRouteSchemeColor() {
        return '#' + routesSchemesColors[routesSchemesOverlaysCount++ % routesSchemesColors.length];
    }


    /**
     * Centers map on lnu building.
     */
    function createMapCenterControl() {
        var centerMapControl = {
            position: 'right_bottom',
            title: centerText,
            disableDefaultStyles: true,
            classes: 'btn-glyph glyph-university',
            events: {
                click: focusOnLnu
            }
        };
        map.addControl(centerMapControl);
    }

    /**
     * Centers map on your current location.
     */
    function createGeolocationControl() {
        var geolocationControl = {
            position: 'right_bottom',
            title: "Місцезнаходження",
            disableDefaultStyles: true,
            classes: 'btn-glyph glyph-mylocation',
            events: {
                click: findMyGeolocation
            }
        };
        map.addControl(geolocationControl);
    }

    /**
     * Slide side menu with warkers toggles list 
     * and marker search combobox with autocomplete.
     */
    function cleateSlideMenuControl() {
        var markersListItems = "";
        for (var markerType in markers) {
            if (markers[markerType].hasOwnProperty('showInMenu') && !markers[markerType].showInMenu)
                continue;

            markersListItems +=
                '<li>' +
                '<label class="' + markerType + '">' +
                '<input id="' + markerType + '" type="checkbox">' + markers[markerType].title + '</label>' +
                '</li>';
        }

        var innerHTML =
            '<div><input id="dr-trigger" class="dr-trigger" type="checkbox"><label class="card" for="dr-trigger"><span class="dr-icon"></span>Позначки</label></div>' +
            '<form>' +
            '<div class="markers-select"><select id="markers-select" data-placeholder="Пошук"></select></div>' +
            '<fieldset id="marker-toggles" class="marker-toggles">' +
            '<ul>' +
            markersListItems +
            '</ul>' +
            '</fieldset>' +
            '</form>';

        var slideMenuControl = {
            position: 'left_top',
            content: innerHTML,
            disableDefaultStyles: true,
            classes: 'dr-menu',
            id: 'dr-menu',
            style: {
                'z-index': 100
            }
        };

        var slideControlHandle = map.addControl(slideMenuControl);

        // Initializes event listeners for checkboxes in slide menu when first opened.   
        google.maps.event.addDomListenerOnce(slideControlHandle, 'click', initialize);
    }

    /**
     * Slide menu with list of buttons
     * which toggle route overlays.
     */
    function createRoutesControl() {
        var routeToggles = "";

        for (var routeType in routes) {
            var routeData = routes[routeType].routes;
            var innerDiv = "";

            for (var i = 0; i < routeData.length; i++) {
                var checked = (routeData[i].hasOwnProperty('visible') && !routeData[i].visible) ? '' : 'checked';

                innerDiv += '<span class="route"><input ' + checked + ' type="checkbox" id="' + routeData[i].id + '">' +
                    '<label for="' + routeData[i].id + '">' + routeData[i].title + '</label></span>';
            }

            routeToggles += '<div class="routes-group"><p class="routes-group-title">' + routes[routeType].title + ':</p>' + innerDiv + '</div>';
        }

        var routesControl = {
            position: 'right_top',
            content: '<p class="routes-title">Схеми маршрутів</p>' + '<div class="route-groups">' + routeToggles + '</div>',
            disableDefaultStyles: true,
            classes: 'card routes-control',
            id: 'routes-control'
        };

        map.addControl(routesControl);
    }

    /**
     * Perform geolocation
     * and add marker to map.
     */
    function findMyGeolocation() {
        GMaps.geolocate({
            success: function(position) {
                // center to your location
                map.setCenter(position.coords.latitude, position.coords.longitude);

                // add marker
                var location = map.addMarker({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    title: 'Your position',
                    icon: {
                        size: markerSize,
                        url: absoluteImagesPath + 'img/location.png'
                    },
                    animation: google.maps.Animation.DROP,
                    infoWindow: {
                        content: 'Ваше місцезнаходження'
                    }
                });

                google.maps.event.trigger(location, 'click');
            },
            error: function(error) {
                alert('Geolocation failed: ' + error.message);
            },
            not_supported: function() {
                alert("Your browser does not support geolocation");
            }
        });
    }

    /**
     * Center on lnu building
     * and zoom in
     */
    function focusOnLnu() {
        map.panTo(lnu);
        map.setZoom(17);
        var l = map.addMarker({
            lat: lnu.lat(),
            lng: lnu.lng(),
            title: centerText,
            infoWindow: {
                content: centerText
            }
        });
        google.maps.event.trigger(l, 'click');
    }

    /**
     * Draw building-shaped polygons on map
     * using specified coordinates
     * and bind click events on polygons.
     */
    function drawBuildingOverlays() {
        var i;

        var markersDataTempDictionary = [];
        for (var markerCategoty in markers) {
            var data = markers[markerCategoty].markerData;

            for (i = 0; i < data.length; i++) {
                markersDataTempDictionary[data[i].id] = {
                    category: markerCategoty,
                    data: data[i]
                };
            }
        }

        for (i = 0; i < buildings.data.length; i++) {
            var building = buildings.data[i];
            var style = building.style ? buildings.styles[building.style] : buildings.styles['default'];

            var poly = map.drawPolygon({
                paths: building.path,
                strokeColor: style.strokeColor,
                strokeOpacity: style.strokeOpacity,
                strokeWeight: style.strokeWeight,
                fillColor: style.fillColor,
                fillOpacity: style.fillOpacity
            });

            // poly click event
            if (building.markers) {
                var boundMarkers = [];

                for (var j = 0; j < building.markers.length; j++) {
                    var boundMarker = markersDataTempDictionary[building.markers[j]];
                    boundMarkers.push(boundMarker);
                }

                poly.addListener('click', createPolygonClickHandler(boundMarkers));
            }
        }

        // clear array and pray for GC one more time
        markersDataTempDictionary.length = 0;
    }

    function initRoutesOverlays() {
        for (var routeType in routes) {
            var routeData = routes[routeType].routes;

            for (var i = 0; i < routeData.length; i++) {
                var route = map.drawPolyline({
                    path: routeData[i].path,
                    strokeColor: getRouteSchemeColor(),
                    strokeOpacity: 0.6,
                    strokeWeight: 7
                });

                route.setVisible(false);
                route.addListener('click', createRouteClickHandler(routeData[i]));

                routesBag[routeData[i].id] = route;
            }
        }
    }

    function createPolygonClickHandler(boundMarkers) {
        "use strict";
        return function() {
            // hide already shown markers
            hideMarkers(selectedBuldingMarkers);

            // clear array and pray for GC
            selectedBuldingMarkers.length = 0;

            for (var i = 0; i < boundMarkers.length; i++) {
                if (boundMarkers[i]) {
                    selectedBuldingMarkers.push(
                        addMarker(boundMarkers[i].data, boundMarkers[i].category)
                    );
                }
            }

            showMarkers(selectedBuldingMarkers);

            // open only first info windows if there're a lot of bound markers
            google.maps.event.trigger(selectedBuldingMarkers[0], 'click');
        };
    }

    /**
     * Show info window
     * for clicked route overlay.
     */
    function createRouteClickHandler(routeData) {
        "use strict";
        return function(e) {
            infoWindow.setPosition(e.latLng);
            infoWindow.setContent(routeData.description);
            infoWindow.open(map.map);
        };
    }

    /**
     * Show / hide route overlay when route checkbox checked. 
     */
    function createRouteToggleClickHandler(routeId) {
        "use strict";
        return function() {
            routesBag[routeId].setVisible(this.checked);
        };
    }

    /**
     * markerToggles event handler.
     * @param {String} markerGroup - Marker group name defined in markersBag.
     */
    function createMarkerToggleClickHandler(markerGroup) {
        "use strict";
        return function() {
            if (this.checked) {
                if (markersBag[markerGroup] === undefined) {
                    loadMarkersGroup(markerGroup);
                } else {
                    showMarkers(markersBag[markerGroup]);
                }
            } else {
                hideMarkers(markersBag[markerGroup]);
            }
        };
    }

    /**
     * Loop through markersBag property names,
     * load corresponding marker group from markers.js,
     * and bind click event to checkboxes in markerToggles form.
     */
    function loadMarkersGroup(markerGroup) {
        // create marker bag for this type
        markersBag[markerGroup] = [];
        var group = markers[markerGroup];

        for (var i = 0; i < group.markerData.length; i++) {
            var marker = addMarker(group.markerData[i], markerGroup);
            // cache marker
            markersBag[markerGroup].push(marker);
        }
    }

    /**
     * Create marker from marker data object,
     * shows it on map and return it.
     * 
     * @param {Object} markerData - Marker options.
     * @param {string} markerData.name - Marker name.
     * @param {Object} markerData.lat - Marker latitude.
     * @param {Object} markerData.lng - Marker longitude.
     * @param {string} [markerData.customIconUrl] - Override default icon of marker group.
     * @param {string} [markerData.href] - Put link in info window with marker name.
     * @param {string} markerGroup - The group marker belongs to.
     */
    function addMarker(markerData, markerGroup) {
        var infoWindowContent = '<div class="info-window">';
        infoWindowContent += markerData.hasOwnProperty('href') ?
            '<a target="blank" class="title" href="' + markerData.href + '">' + markerData.name + '</a>' :
            '<div class="title">' + markerData.name + '</div>';

        if (markerData.hasOwnProperty('content')) {
            infoWindowContent += '<div class="content">' + markerData.content + '</div>';
        }

        infoWindowContent += '</div>';

        var markerOptions = {
            lat: markerData.lat,
            lng: markerData.lng,
            title: markerData.name,
            icon: {
                size: markerSize,
                url: absoluteImagesPath +
                    (markerData.hasOwnProperty('customIconUrl') ? markerData.customIconUrl : markers[markerGroup].markerIconUrl)
            },
            animation: google.maps.Animation.DROP,
            infoWindow: {
                content: infoWindowContent
            }
        };

        return map.addMarker(markerOptions);
    }

    /**
     * Hide markers on map.
     * @param {Array} markers Google.maps.marker array
     */
    function hideMarkers(markers) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
    }

    /**
     * Show markers on map.
     * @param {Array} markers Google.maps.marker array
     */
    function showMarkers(markers) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map.map);
            markers[i].setAnimation(google.maps.Animation.DROP);
        }
    }

    /**
     * Show/hide all route polylines.
     * Sid menu checkbox listener.
     */
    function toggleRoutesOverlays() {
        $routesControl.slideToggle(200);

        for (var routeId in routesBag) {
            // set route visible if marker checked and specific route button checked
            routesBag[routeId].setVisible(this.checked && document.getElementById(routeId).checked);
        }
    }

    /**
     * Show marker selected in search box input.
     */
    function onMarkerSearchChanged() {
        var $selected = $markersSelect.find(":selected");
        var groupName = $selected.data('markerGroup');
        var index = $selected.data('indexInGroup');

        var markerData = markers[groupName].markerData[index];

        var marker = addMarker(markerData, groupName);
        map.setZoom(17);
        map.panTo(marker.position);

        google.maps.event.trigger(marker, 'click');
    }

    /**
     * Called once when slide menu button
     * is clicked for the first time.
     * 
     * Used to ensure that map custom controls are loaded
     * and events can be hooked.
     */
    function initialize() {

        // cache DOM
        $routesControl = $('#routes-control');
        $markersSelect = $('#markers-select');
        $routesToggle = $('#routes');

        /*
         * INIT SLIDE MENU EVENTS
         */
        var $menu = $('#dr-menu'),
            $trigger = $('#dr-trigger');

        $trigger.click(function(event) {
            $menu.toggleClass('dr-menu-open');
            $('#dr-trigger + label').toggleClass('card');
        });


        /*
         * INIT MARKER TOGGLES EVENTS
         */
        var toggles = document.getElementById("marker-toggles").getElementsByTagName('input');

        for (var i = 0; i < toggles.length; i++) {
            // uncheck box on refresh
            toggles[i].checked = false;

            toggles[i].addEventListener("click", createMarkerToggleClickHandler(toggles[i].id));

            if (toggles[i].id === "routes") {
                toggles[i].addEventListener("click", toggleRoutesOverlays);
            }
        }

        /*
         * Hook up route toggles click events
         */
        var routeToggles = document.getElementById("routes-control").getElementsByTagName('input');
        for (i = 0; i < routeToggles.length; i++) {
            routeToggles[i].onclick = createRouteToggleClickHandler(routeToggles[i].id);
        }

        initMarkersSearchInput();
    }

    /**
     * Loops through markers.js,
     * creates option group for every marker group
     * and fills it with markers.
     */
    function initMarkersSearchInput() {
        var $markersSearch = $('#markers-select');

        $markersSearch.append('<option></option>');

        // fill select with options from markers.js
        for (var markerType in markers) {
            var markerGroup = markers[markerType];

            // create option group from marker type
            var $optGroup = $('<optgroup>', { label: markerGroup.title });

            // fill group with options from marker group data
            for (var i = 0; i < markerGroup.markerData.length; i++) {
                $optGroup.append($('<option>', {
                    text: markerGroup.markerData[i].name,
                    data: {
                        markerGroup: markerType,
                        indexInGroup: i
                    }
                }));
            }
            $markersSearch.append($optGroup);
        }

        $markersSearch
        // create autocomplete
            .chosen(chosenAutocompleteOptions)
            // add onchange event
            .change(onMarkerSearchChanged);
    }

    $(document).ready(function() {
        if ($('#lnu-map').length === 0)
            return;

        map = new GMaps(mapOptions);

        // create custom map controls
        createGeolocationControl();
        createMapCenterControl();
        cleateSlideMenuControl();
        createRoutesControl();

        // draw buildings polygons
        drawBuildingOverlays();

        // predraw routes
        initRoutesOverlays();
    });
})(jQuery);