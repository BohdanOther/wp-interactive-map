<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       vk.com/lhospital
 * @since      1.0.0
 *
 * @package    Lnu_Map
 * @subpackage Lnu_Map/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
