<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       vk.com/lhospital
 * @since      1.0.0
 *
 * @package    Lnu_Map
 * @subpackage Lnu_Map/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Lnu_Map
 * @subpackage Lnu_Map/public
 * @author     bother <harpandguitar@gmail.com>
 */
class Lnu_Map_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( 'chosen-css', plugin_dir_url( dirname(__FILE__) ) . 'includes/libs/chosen/chosen.min.css', array());	
		wp_enqueue_style( 'lnu-map-css', plugin_dir_url( dirname(__FILE__) ) . 'includes/css/lnu-map.min.css', array('chosen-css'), $this->version, 'all' );	
		
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/lnu-map-public.css', array('lnu-map-css'), $this->version, 'all' );
	
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_register_script( 'google-maps', 'http://maps.googleapis.com/maps/api/js?sensor=false', array(), false, true);
		wp_register_script( 'chosen-js', plugin_dir_url( dirname(__FILE__) ) . 'includes/libs/chosen/chosen.jquery.min.js', array( 'jquery' ), false, true );
		wp_register_script( 'gmaps-js', plugin_dir_url( dirname(__FILE__) ) . 'includes/libs/gmaps/gmaps.min.js', array( 'jquery', 'google-maps' ), false, true );		

		wp_register_script( 'lnu-map-public', plugin_dir_url( __FILE__ ) . 'js/lnu-map-public.js', array( 'jquery', 'google-maps', 'gmaps-js', 'chosen-js' ), $this->version, true );
		
		// Localize the script with new data
		$data = array(
			'zoom' => get_option( 'lnu_map_zoom' ),
			'markers' => get_option( 'lnu_map_markers' ),
			'buildings' => get_option( 'lnu_map_buildings' ),
			'routes' => get_option( 'lnu_map_routes' ),
			'centerCoords' => get_option( 'lnu_map_center_coord' ),
			'centerText' => get_option( 'lnu_map_center_text' )

		);
		wp_localize_script( 'lnu-map-public', 'phpData', $data );
		
	}

}
